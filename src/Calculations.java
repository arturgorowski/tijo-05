public class Calculations {

    public static Point2D positionGeometricCenter(Point2D[] points) {
        double x = 0;
        double y = 0;
        for(Point2D point : points){
            x += point.getX();
            y += point.getY();
        }
        x = x/points.length;
        y = y/points.length;
        return new Point2D(x,y);
    }

    public static Point2D positionCenterOfMass(MaterialPoint2D[] materialPoints) {
        double x = 0;
        double y = 0;
        double mass = 0;
        for(MaterialPoint2D point : materialPoints){
            x += point.getX();
            y += point.getY();
            mass += point.getMass();
        }

        return new MaterialPoint2D(x/mass,y/mass,mass);
    }
}